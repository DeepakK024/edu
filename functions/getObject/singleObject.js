const model = require('./../../model/index');

async function getRole(roleId) {
    try {
        let role = {}
        role['id'] = roleId;
        let singleRole = await model['role'].findOne({
            where: role
        });
        return singleRole;
    }
    catch (e) {
        return e;
    }
}

async function getToken(userId, deviceId) {
    try {
        let token = {}
        token['userId'] = userId;
        token['deviceId'] = deviceId
        let singleToken = await model['user_session'].findOne({
            where: token
        });
        return singleToken;
    }
    catch (e) {
        return e;
    }
}


module.exports = { getRole, getToken }