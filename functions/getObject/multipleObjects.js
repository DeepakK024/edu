const { Op, Model } = require('sequelize');
const model = require('./../../model/index');

async function getPriviliges(roleId) {
    try {
        let ids = [];
        let role = {}
        role['roleId'] = roleId
        let att = {}
        att['columnNames'] = ["priviligesId"]
        const thisUserPriviliges = await model['user_priviliges'].findAll({
            where: role,
            raw: true,
            attributes: att.columnNames
        });
        for (let i = 0; i < thisUserPriviliges.length; i++) {
            ids.push(thisUserPriviliges[i].priviligesId);
        }

        let priviligesNames = []
        let priviligeId = {}
        priviligeId['id'] = { [Op.in]: ids }
        let attPriviliges = {}
        attPriviliges['columnNames'] = ["priviliges"]
        const data = await model['master_priviliges'].findAll({
            where: priviligeId,
            raw: true,
            attributes: attPriviliges.columnNames
        });
        for (let i = 0; i < data.length; i++) {
            priviligesNames.push(data[i].priviliges);
        }
        return priviligesNames;
    }
    catch (e) {
        console.log("error ", e);
        return e;
    }
}

module.exports = { getPriviliges }