const crypto = require('crypto');
const moment = require('moment');
const { authObject } = require('../objects/sessionToken');
const { insertUser } = require('../insert/signup');

async function createToken(userId, deviceId, token) {
    //let token = crypto.randomBytes(64).toString('hex');
    let dateExpire = moment(new Date(Date.now() + (24 * 60 * 60 * 1000))).format('YYYY-MM-DD HH:mm:ss');

    const tokenObject = await authObject(userId, token, dateExpire, deviceId);
    const result = await insertUser(tokenObject, "user_session");
    return result;
}

module.exports = { createToken }