const model = require('../../model/index');
const { Op, Model } = require('sequelize');
const sequelize = require('../../config/sequelize');
const { userObject } = require('../objects/userObject');
const { employerObject } = require('../objects/employerObject');
const { employeeObject } = require('../objects/employeeObject');
const { serviceProviderObject } = require('../objects/serviceProviderObject');
const { languageObject, expertiseObject } = require('../objects/languagesObject');

// var cls = require('continuation-local-storage'),
// const namespace = cls.createNamespace('my-very-own-namespace');

// var Sequelize = require('sequelize');
// Sequelize.cls = namespace;

// new Sequelize(....);

async function signup(body) {
    try {
        if (body.type == "employee") {
            const userModel = await userObject(body.data);
            userModel['roleId'] = 3;
            const user = await insertUser(userModel, "User");
            if (user) {
                var employee = await employeeObject(body.data);
                employee['userId'] = user.id;
                const result = await insertUser(employee, "edu_employee");
                if (result) {
                    console.log("result ============ ", result)
                    const insertLanguages = await languagesInsertion(body.data.languages, result['dataValues'].id);
                }

                return result;
            }
            // else {
            //     console.log("invalid user");
            //     return user.error;
            // }

        }
        else if (body.type == "employer") {
            const userModel = await userObject(body.data);
            userModel['roleId'] = 2;
            const user = await insertUser(userModel, "User");
            if (user) {
                var employer = await employerObject(body.data);
                employer['userId'] = user.id;
                const result = await insertUser(employer, "Edu_Employer");
                return result;
            }
            // else {
            //     console.log("invalid user");
            //     return user;
            // }
        }
        else if (body.type == "serviceProvider") {
            const userModel = await userObject(body.data);
            userModel['roleId'] = 4;
            const user = await insertUser(userModel, "User");
            if (user) {
                var serviceProvider = await serviceProviderObject(body.data);
                serviceProvider['userId'] = user.id;
                const result = await insertUser(serviceProvider, "edu_service_provider");
                if (result) {
                    console.log("result ============ ", result)
                    const insertExpertise = await expertiseInsertion(body.data.expertise, result['dataValues'].id, 4);
                }
                return result;
            }
            // else {
            //     console.log("invalid user");
            //     return user.error;
            // }
        }
        else {
            console.log("invalid type of signup ")
            return false;
        }
    }
    catch (err) {
        console.log("error %%%%%%%%%%%%%%%%%%%%% ", err)
        throw err;
    }
}

async function insertUser(user, modelName) {
    try {
        var newmodels = model[modelName]
        var resolve = await newmodels.create(user)
        return resolve
    }
    catch (e) {
        console.log("error ==== ", e);
        throw e;
        //return e;
    }

}
async function languagesInsertion(languages, userId) {
    console.log("total languages ", languages.length)
    return sequelize.transaction(async function (t) {

        // chain all your queries here. make sure you return them.
        for (let i = 0; i < languages.length; i++) {
            const languageObj = await languageObject(languages[i].name, userId);
            var newmodels = model['user_languages']
            var resolve = await newmodels.create(languageObj, { transaction: t })
            //return resolve
        }

    }).then(function (result) {
        // Transaction has been committed
        // result is whatever the result of the promise chain returned to the transaction callback
        console.log("commited ", result)
    }).catch(function (err) {
        // Transaction has been rolled back
        // err is whatever rejected the promise chain returned to the transaction callback
        console.log("error ", err)
    });
}


async function expertiseInsertion(expertises, userId, roleId) {
    console.log("total expertises ", expertises.length)
    return sequelize.transaction(async function (t) {

        // chain all your queries here. make sure you return them.
        for (let i = 0; i < expertises.length; i++) {
            const expertiseObj = await expertiseObject(expertises[i], userId, roleId);
            var newmodels = model['edu_expertise']
            var resolve = await newmodels.create(expertiseObj, { transaction: t })
            //return resolve
        }

    }).then(function (result) {
        // Transaction has been committed
        // result is whatever the result of the promise chain returned to the transaction callback
        console.log("commited ", result)
    }).catch(function (err) {
        // Transaction has been rolled back
        // err is whatever rejected the promise chain returned to the transaction callback
        console.log("error ", err)
    });
}

module.exports = { signup, insertUser }