const { applyJobObject, saveJobObject, applyServiceObject, saveServiceObject } = require('../objects/applyJobObject');
const { applyForView } = require('../../middleware/applyCheck');
const { insertUser } = require('../insert/signup');
const { applyTrainingObject } = require('../objects/applyTrainingObject');
const { Op, Model } = require('sequelize');
const model = require('./../../model/index');


async function insertAction(formMapper, data, res, req) {

    //check for already applied 
    let alreadyAdded;
    let validator = JSON.parse(JSON.parse(formMapper.validator));
    if (validator['alreadyApplied']) {
        alreadyAdded = await applyForView(data, formMapper, req)
        if (alreadyAdded.count !== 0) {
            return false;
        }
        else {
            const dataResults = await applyOnView(data, formMapper);
            return dataResults;
        }
    }
}


async function applyOnView(fields, formMapper) {
    const view = formMapper.view;
    const modelName = formMapper.fromTable;
    console.log("modelName-=========== ", modelName)
    let status;
    switch (view) {
        case 'Apply Job':
            console.log("in applyOnView Apply Job")
            const applyJob = await applyJobObject(fields);
            status = await insertUser(applyJob, modelName);
            break;
        case 'Register Development':
            console.log("in applyOnView Register Development")
            const applyTraining = await applyTrainingObject(fields);
            status = await insertUser(applyTraining, modelName);
            break;
        case 'Favourite Job':
            console.log("in applyOnView Favourite Job")
            const saveJob = await saveJobObject(fields);
            status = await insertUser(saveJob, modelName);
            break;
        case 'Submit Request':
            console.log("in applyOnView Submit Request")
            //const saveJob = await saveJobObject(fields);
            status = await changeStatus(fields.id, modelName);
            break;

        case 'Apply Service':
            console.log("in applyOnView Apply Service")
            const applyService = await applyServiceObject(fields);
            status = await insertUser(applyService, modelName);
            break;
        case 'Favourite Service':
            console.log("in applyOnView Favourite Service")
            const saveService = await saveServiceObject(fields);
            status = await insertUser(saveService, modelName);
            break;
        default:
            console.log("applyOnView invalid view")
    }
    return status;
}

async function changeStatus(idTraining, modelName) {
    try {
        const foundItem = await model[modelName].findOne({ where: { "id": idTraining } });
        if (foundItem) {
            const item = await model[modelName].update({ "statusId": 2 }, { where: { "id": idTraining } });
            return { item, created: false };
        }
        return "No such developments"

    }
    catch (e) {
        console.log("errorr ========= ", e)
        return e;
    }
}

module.exports = { insertAction }