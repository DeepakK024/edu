
async function employeeObject(data) {
    let employee = {
        "firstName": data.firstName,
        "middleName": data.middleName,
        "lastName": data.lastName,
        "country": data.country,
        "state": data.state,
        "city": data.city,
        "address": data.address,
        "dob": data.dob,
        "gender": data.gender,
        "about": data.about,
        "displayName": data.displayName
    }
    return employee;
}

module.exports = { employeeObject }