
async function languageObject(languageId, userId) {
    let language = {
        "language": languageId,
        "userId": userId
    }
    return language;
}

async function expertiseObject(expertiseId, userId, roleId) {
    let expertise = {};
    if (roleId == 4) {
        expertise = {
            "masterExpertiseId": expertiseId,
            "serviceProviderId": userId
        }
    }
    else {
        expertise = {
            "masterExpertiseId": expertiseId,
            "employeeId": userId
        }
    }
    return expertise;
}

module.exports = { languageObject, expertiseObject }