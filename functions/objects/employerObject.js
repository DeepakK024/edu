
async function employerObject(data) {
    let employer = {
        "name": data.name,
        "mission": data.mission,
        "goal": data.goal,
        "hrcontact": data.hrcontact,
        "country": data.country,
        "state": data.state,
        "city": data.city,
        "address": data.address
    }
    return employer;
}

module.exports = { employerObject }