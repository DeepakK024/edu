
async function serviceProviderObject(data) {
    let serviceProvider = {
        "firstName": data.firstName,
        "middleName": data.middleName,
        "lastName": data.lastName,
        "dob": data.dob,
        "gender": data.gender,
        "displayName": data.displayName,
        "organization": data.organization
    }
    return serviceProvider;
}

module.exports = { serviceProviderObject }