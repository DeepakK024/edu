const moment = require('moment');

async function applyJobObject(data) {
    let applyJob = {
        "jobId": data.jobId,
        "employeeId": data.employeeId,
        "employerId": data.employerId,
        "fileId": data.fileId
    }
    return applyJob;
}

async function saveJobObject(data) {
    let saveJob = {
        "jobId": data.jobId,
        "employeeId": data.employeeId,
        "employerId": data.employerId
    }
    return saveJob;
}

async function applyServiceObject(data) {
    let nowTimeStamp = moment(Date.now()).format('YYYY-MM-DD');
    let applyService = {
        "serviceId": data.serviceId,
        "serviceProviderId": data.serviceProviderId,
        "employerId": data.employerId,
        "appliedDate": nowTimeStamp
    }
    return applyService;
}

async function saveServiceObject(data) {
    let saveService = {
        "serviceId": data.serviceId,
        "serviceProviderId": data.serviceProviderId,
        "employerId": data.employerId
    }
    return saveService;
}

module.exports = { applyJobObject, saveJobObject, applyServiceObject, saveServiceObject }