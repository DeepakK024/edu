const bcrypt = require('bcrypt');
const saltRounds = 10;

// async function passwordHash(password) {
//     const hashh = await bcrypt.hash(password, saltRounds)
//     console.log("hash===================", hashh);
// }


async function userObject(data) {
    if (data.password === data.confirmPassword) {
        const hash = await bcrypt.hash(data.password, saltRounds);
        let user = {
            "photo": data.photo,
            "email": data.email,
            "password": hash,
            "confirmPassword": hash,
            "phoneNumber": data.phoneNumber
        }
        return user;
    }
    else {
        console.log("passwords donot match");
        return false;
    }

}

module.exports = { userObject }