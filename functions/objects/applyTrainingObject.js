
async function applyTrainingObject(data) {
    let applyTraining = {
        "trainingId": data.trainingId,
        "hostingEmployeeId": data.hostingEmployeeId,
        "participantEmployeeId": data.participantEmployeeId
    }
    return applyTraining;
}

module.exports = { applyTrainingObject }