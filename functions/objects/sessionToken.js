
async function authObject(userId, sessionToken, expiryDate, deviceId) {
    let auth = {
        "sessionToken": sessionToken,
        "deviceId": deviceId,
        "expiryTime": expiryDate,
        "userId": userId
    }
    return auth;
}

module.exports = { authObject }