const model = require('./../../model/index');


async function getAndDeleteToken(token, tokenAuth) {
    try {
        const foundItem = await model['user_session'].findOne({ where: { sessionToken: tokenAuth, deviceId: token.deviceId } });
        if (!foundItem) {
            return { item, deleted: false };
        }
        const item = await model['user_session'].destroy({ where: { sessionToken: tokenAuth, deviceId: token.deviceId }, force: true });
        return { item, deleted: true };
    }
    catch (e) {
        console.log("error ======== ", e)
        return e;
    }
}

module.exports = { getAndDeleteToken }