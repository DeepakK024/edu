const model = require('./../../model/index');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const { createToken } = require('../insert/allInsertType');
const crypto = require('crypto');
require('dotenv').config();
const jwt = require("jsonwebtoken");

const { getRole, getToken } = require('../getObject/singleObject');
const { getPriviliges } = require('../getObject/multipleObjects');


//let token = crypto.randomBytes(64).toString('hex');




async function userCreds(data, modelName) {
    try {
        // let token = crypto.randomBytes(64).toString('hex');
        // console.log("token 64bit ============ ", token)
        let filter = {}
        filter['email'] = data.email;
        //filter['password'] = data.password;
        let user = await model[modelName].findOne({
            where: filter,
        });

        // role 
        const subTableName = await getRoleUser(user['dataValues'].roleId)
        /// for password compare
        const verifiedUser = await compareBothPass(user, data.password);

        ///
        if (!verifiedUser) { throw new Error("User is not verified") }
        if (!user['dataValues']['emailVerified']) { throw new Error("Email not verified") }
        await checkTokenExist(user['dataValues'].id, data.device);
        const role = await getRole(user['dataValues'].roleId);
        const token = await insertToken(user['dataValues'].id, data.device, role['dataValues'].role, role['dataValues'].id);
        if (!token) { throw new Error("token not created") }
        let typeUser = await retrieveUser(user['dataValues'].id, subTableName);
        typeUser['dataValues']['phoneNumber'] = user['dataValues'].phoneNumber;
        typeUser['dataValues']['photo'] = await getRelationalData(user['dataValues'].photo);
        typeUser['dataValues']['sessionToken'] = token['dataValues'].sessionToken;
        typeUser['dataValues']['priviliges'] = await getPriviliges(user['dataValues'].roleId);
        typeUser['dataValues']['email'] = data.email;
        typeUser['dataValues']['role'] = role['dataValues'].role;
        return typeUser;
    }
    catch (e) {
        console.log("error========== ", e);
        throw e;
        //return e;
    }
}

async function getRelationalData(id) {
    try {
        console.log("intoRealtion func  ", id)
        let fileId = {}
        fileId['id'] = id;
        let user = await model['edu_master_file'].findOne({
            where: fileId,
        });
        console.log("returning this ", user)
        return user['dataValues'].path;

    }
    catch (e) {
        return e;
    }
}

async function retrieveUser(userId, modelName) {
    //console.log("userId ", userId)

    let att = {}
    //att['columnNames'] = ["photo", "phoneNumber"]
    //attributes: att.columnNames
    let filter = {}
    filter['userId'] = userId;
    let user = await model[modelName].findOne({
        where: filter,
    });
    console.log("userrrrrrrrrrrr ", user)
    return user;
}

async function getRoleUser(roleId) {
    try {
        const roleName = await getRole(roleId);
        console.log('roleName======== ', roleName)
        const roleType = await getUserModel(roleName.role)
        return roleType;
    }
    catch (err) {
        return err;
    }
}



async function getUserModel(type) {
    const view = type;
    let status;
    switch (view) {
        case 'Employee':
            status = "edu_employee";
            break;
        case 'Employer':
            status = "Edu_Employer";
            break;
        case 'Service_Provider':
            status = "edu_service_provider";
            break;
        default:
            console.log("invalid view")
    }
    return status;
}

async function compareBothPass(user, password) {
    //console.log("user============", user);

    const hash = user['dataValues'].password;
    //console.log("hash-=========== ", hash)
    if (bcrypt.compare(password, hash)) {
        //console.log("returning true ")
        return true;
    }
    else {
        //console.log("returning false")
        return false;
    }

}

async function insertToken(userId, deviceId, role, roleId) {
    const tokenJWT = await generateAccessToken(userId, role, roleId, deviceId);
    if (tokenJWT) {
        const tokenInserted = await createToken(userId, deviceId, tokenJWT);
        return tokenInserted;
    }
    return false;
}

async function generateAccessToken(username, role, roleId, deviceId) {
    // expires after half and hour (1800 seconds = 30 minutes)
    //console.log("things==== ", username, process.env.TOKEN_SECRET)
    return jwt.sign({ username, role, roleId, deviceId }, process.env.TOKEN_SECRET, { expiresIn: '24h' });
}
async function checkTokenExist(userId, deviceId) {
    try {
        const isTokenThere = await getToken(userId, deviceId);
        if (isTokenThere['dataValues']) {
            return await deleteAlreadyToken(isTokenThere['dataValues'].id)
        }
        else {
            return false;
        }
    }
    catch (e) {
        return e
    }
}

async function deleteAlreadyToken(tokenId) {
    try {
        let token = {}
        token['id'] = tokenId;
        const delToken = await model['user_session'].destroy({
            where: token
        });
        return delToken;
    }
    catch (e) {
        return e
    }
}
// async function isEmailVerified(userId) {
//     try {
//         let emailUser = {}
//         emailUser['id'] = userId;

//     }
//     catch (e) {
//         return e;
//     }
// }

module.exports = { userCreds, getUserModel }