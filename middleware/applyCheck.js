const model = require('../model/index');
const { Op, Model } = require('sequelize');

async function applyForView(fields, formMapper, req) {
    const view = formMapper.view;
    let role = req.user.role;
    let status;
    switch (view) {
        case 'Apply Job':
            console.log("in applyForView Apply Job")
            status = await checkData(fields, formMapper, ["jobId", "employeeId"])
            break;
        case 'Register Development':
            if (role === "serviceProvider") {
                console.log("in applyForView Register Development of serviceProvider")
                status = await checkData(fields, formMapper, ["trainingId", "serviceProviderId"])
                break;
            }
            else {
                console.log("in applyForView Register Development of employee")
                status = await checkData(fields, formMapper, ["trainingId", "participantEmployeeId"])
                break;
            }
        case 'Favourite Job':
            console.log("in applyForView Save Job")
            status = await checkData(fields, formMapper, ["jobId", "employeeId"])
            break;
        case 'Submit Request':
            console.log("in applyForView Submit Request")
            status = await checkSubmitted(formMapper, fields.id)
            break;
        case 'Apply Service':
            console.log("in applyForView Apply Service")
            status = await checkData(fields, formMapper, ["serviceId", "serviceProviderId"])
            break;
        case 'Favourite Service':
            console.log("in applyForView Favorite Service")
            status = await checkData(fields, formMapper, ["serviceId", "serviceProviderId"])
            break;
        default:
            console.log("applyForView invalid view")
    }
    return status;
}

async function checkData(fields, formMapper, colmNames) {
    try {
        let filter = {};
        for (let i = 0; i < colmNames.length; i++) {
            filter[colmNames[i]] = fields[colmNames[i]];
        }
        const data = await model[formMapper.fromTable].findAndCountAll({
            where: filter,
            raw: true
        });

        return data;
    }
    catch (error) {
        return error;
    }

}

async function checkSubmitted(formMapper, id) {
    try {
        let trainingId = {}
        trainingId['id'] = id;
        let getTraining = await model[formMapper.fromTable].findOne({
            where: trainingId
        });
        if (getTraining['dataValues'].statusId === 1) {
            trainingId['count'] = 0;
            return trainingId
        }
        else {
            trainingId['count'] = 1;
            return trainingId
        }
    }
    catch (error) {
        return error
    }
}

// async function getObject(fields, colName) {
//     let value;
//     console.log("=========== ", fields, colName)
//     for (let i = 0; i < fields.length; i++) {
//         if (fields[i] === colName) {
//             console.log("=========match")
//             value = fields[i]
//         }
//     }
//     return value;
// }

module.exports = { applyForView }