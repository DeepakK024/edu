const multer = require('multer');


const fs = require('fs');
const path = require('path');
// var router = require('../../public/profileVideos')
const relativePath = path.resolve(__dirname, '../public/profileVideos');

var videoFile = {
    "file": ""
};

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        dir = `${relativePath}`;
        fs.mkdir(dir, err => {
            if (err && err.code != 'EEXIST') throw err;
        })
        cb(null, `${dir}`)
    },
    filename: (req, file, cb) => {
        console.log("-------", file)
        var ext = file.originalname.substring(file.originalname.lastIndexOf("."));
        videoFile[file.fieldname] = Date.now() + ext;


        cb(null, videoFile[file.fieldname])


    }
});


const upload = multer({ //multer settings
    storage: storage,
    fileFilter: function (req, file, callback) {
        console.log("============", file)
        var ext = path.extname(file.originalname);
        // if (ext !== '.jpg' && ext !== '.PNG') {
        //     callback({ message: 'Invalid format' })
        // }
        callback(null, true)
    }
}).fields([{ name: "file" }]);

module.exports = upload