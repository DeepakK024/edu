var fs = require('fs');
require('dotenv').config();
var AWS = require('aws-sdk');



async function preSignedURL(fileKey) {
    try {
        AWS.config.update({
            accessKeyId: process.env.ACCESS_KEY_ID,
            secretAccessKey: process.env.SECRET_ACCESS_KEY,
            region: 'us-east-1',
            signatureVersion: 'v4'
        });

        const s3 = new AWS.S3()
        const myBucket = process.env.BUCKET
        const myKey = fileKey

        const signedUrlExpireSeconds = 518400

        const url = s3.getSignedUrl('getObject', {
            Bucket: myBucket,
            Key: myKey,
            Expires: signedUrlExpireSeconds
        });

        console.log(url);
        return url;
    }
    catch (e) {
        return e;
    }
}

module.exports = { preSignedURL }