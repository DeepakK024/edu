'use strict'
// const Joi = require('joi');
const {celebrate,Joi} = require('celebrate');

 const signUpValidate = celebrate({
     body: Joi.object().keys({
         firstName: Joi.string().required(),
         lastName:Joi.string().required(),
         email:Joi.string().email().required()
     }),
 });
 
 module.exports ={
    signUpValidate
 }