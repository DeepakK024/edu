const model = require('../../model/index');
const { getAuth } = require('./userAuthorization');
const { Op, Model } = require('sequelize');

module.exports = {
    isPriviliged: async function (req, res, next) {
        try {
            //
            // getUserId
            const currentAuth = await getAuth(req.headers.authorization);
            console.log("currentAuth============= ", currentAuth);
            //getRoleId
            const userRole = await getUser(currentAuth['userId']);
            console.log("userRole=========== ", userRole)
            // compare role with tableName
            const matchedRole = await compareRolePrivilige(userRole['roleId'], req.body.params.tableName);
            console.log("matchedRole================= ", matchedRole);
            if (matchedRole) {
                return next();
            }
            else {
                res.status(401).json({
                    success: false,
                    message: 'Unauthorized View'
                });
            }
        } catch (e) {
            console.log("error")
            console.log(e);
        }
    }
}

async function getUser(userIdAuth) {
    try {
        let userId = {};
        userId['id'] = userIdAuth;
        const data = await model['User'].findOne({
            where: userId
        });
        return data['dataValues'];
    }
    catch (err) {
        return err;
    }

}



async function compareRolePrivilige(roleId, view) {

    try {
        const priviligeObject = await getPriviligeId(view);
        console.log("privilige============== ", priviligeObject)
        if (priviligeObject) {
            try {
                let privilige = {}
                privilige['roleId'] = roleId;
                privilige['priviligesId'] = priviligeObject['dataValues'].id;
                const data = await model['user_priviliges'].findOne({
                    where: privilige
                });
                return data;
            }
            catch (err) {
                return err
            }
        }
    }
    catch (error) {
        return error
    }
}

async function getPriviligeId(viewName) {
    try {
        let view = {};
        view['priviliges'] = viewName; // { [Op.like]: `%${viewName}%` }; //viewName;
        const data = await model['master_priviliges'].findOne({
            where: view
        });
        return data;
    }
    catch (e) {
        return e
    }

}