const model = require('../../model/index');
const jwt = require('jsonwebtoken');
const jwt_decode = require('jwt-decode');

module.exports = {
    isAuthorization: async function (req, res, next) {

        if (req.headers.authorization) {
            try {
                var rows = await module.exports.getAuth(req.headers.authorization);
                if (rows) {
                    let ttl = rows['expiryTime'];
                    let nowTimeStamp = new Date();
                    if (ttl > nowTimeStamp) {
                        return next();
                    }
                    else {
                        res.status(401).json({
                            success: false,
                            message: 'Token is expired'
                        });
                    }
                }
                else {
                    res.status(401).json({
                        success: false,
                        message: 'Token is invalid'
                    });
                }
            } catch (e) {
                console.log("error")
                console.log(e);
            }
        }
        else {
            res.status(401).json({
                success: false,
                message: 'You are not authorized!'
            });
        }
    },
    getAuth: async function (auth) {
        try {
            let token = {};
            token['sessionToken'] = auth;

            const data = await model['user_session'].findOne({
                where: token
            });
            return data['dataValues'];
        } catch (e) {
            console.log("error")
            console.log(e);
        }


    },
    authenticateToken: async function (req, res, next) {
        // Gather the jwt access token from the request header
        console.log("authenticate============?? ")
        const authHeader = req.headers['authorization']
        console.log("auth recievd ============ ", authHeader)
        const token = authHeader && authHeader
        //console.log("token recievd ============ ", token)



        var decoded = jwt_decode(token);

        console.log("decoded============== ==== ", decoded);
        if (token == null) return res.sendStatus(401) // if there isn't any token

        jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
            //console.log("error============ ", err)
            if (err) return res.sendStatus(403)
            console.log("user========== ", decoded)
            req.user = decoded
            next() // pass the execution off to whatever request the client intended
        })
    }
}