const AWS = require('aws-sdk');
const fs = require('fs');

require('dotenv').config();


async function createParams(fileKey, filePath) {
    console.log("filePath ", filePath)
    var params = {
        Bucket: process.env.BUCKET,
        Body: filePath,
        Key: process.env.PATH_ + fileKey,
        // ACL: "public-read",
    };
    console.log("params ", params.Key)
    return params;
}


const s3 = new AWS.S3({
    accessKeyId: process.env.ACCESS_KEY_ID,
    secretAccessKey: process.env.SECRET_ACCESS_KEY,
    region: 'us-east-1'
})

module.exports = { createParams, s3 }