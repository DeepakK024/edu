const model = require('./../../model/index');
const { Op, Model } = require('sequelize');
const { signup } = require('../../functions/insert/signup')

require('dotenv').config();

module.exports = async (req, res) => {
    try {
        const result = await signup(req.body);
        if (result) {
            res.status(200).send({
                message: "Success",
                result
            })
        }

    }
    catch (err) {
        console.log(err);
        res.status(406).send({
            message: err.stack
        })
    }
}