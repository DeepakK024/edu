const { userCreds } = require('../../functions/read/read')
require('dotenv').config();

module.exports = async (req, res) => {
    try {
        const results = await userCreds(req.body, "User")
        if (results) {
            res.status(200).send({
                message: "success",
                results
            })
        }
    }
    catch (err) {
        res.status(406).send({
            message: err.message
        })
    }
}
