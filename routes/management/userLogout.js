require('dotenv').config();
const { getAndDeleteToken } = require('../../functions/read/logout');

module.exports = async (req, res) => {
    try {
        const results = await getAndDeleteToken(req.user, req.headers.authorization);
        if (results.deleted) {
            res.status(200).send({
                message: "success",
                results
            })
        }
    }
    catch (err) {
        res.status(406).send({
            message: err.message
        })
    }
}


//get the token
// delete the token if found.
// 