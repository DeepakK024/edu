var express = require('express');
const { query, _attributes } = require('../../config/sequelize');
const model = require('./../../model/index');
const { Op, Model } = require('sequelize');


module.exports = async (req, res) => {
    try {


        var objectToFill = req.body;

        console.log("object ", objectToFill)

        var tableName = objectToFill.properties.config.tableName;

        var definingColumn = objectToFill.properties.propertyToShow;

        var att = [
            "id",
            definingColumn
        ]

        console.log("att ", att[1])
        if (objectToFill.inputType === "dropdown" && objectToFill.properties.toBeFilled) {
            const data = await model[tableName].findAndCountAll({

                order: [[att[0], 'ASC']],
                raw: true,
                //attributes: att


            });
            console.log("data ", data)
            objectToFill.properties.data = data.rows;
        }
        else {
            console.log("it is already filled")
        }



        //data['attributes'] = att
        res.status(200).send({
            message: "get user data succesfully",
            result: {
                data: objectToFill
            }
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.stack
        })
    }
}