var express = require('express');
const model = require('./../../model/index');

module.exports = async (req,res)=>{
    try{
        const viewPage = req.body.params.tableName;
        const filterMapper = await model['FilterMapper'].findOne({where:{view:viewPage}});
        let filterString = JSON.parse(JSON.parse(filterMapper.filterObject));
        console.log("filterString   ",filterString)
        console.log("JSON format of filterString",filterString);

        res.status(200).send({
            message: "get filter data succesfully",
            result: {
                 filter: filterString
            }
        })
    }catch(err){
        res.status(406).send({
            message: err.stack
        })
    }
}