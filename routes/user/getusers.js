var express = require('express');
const { query, _attributes } = require('../../config/sequelize');
const model = require('./../../model/index');
const { Op, Model } = require('sequelize');


module.exports = async (req, res) => {
    try {
        // find all data query.
        const po = parseInt(req.body.pagination.offset);
        const pl = parseInt(req.body.pagination.limit)
        var sort = req.body.sort;
        const userRole = req.user.roleId;

        if (!sort) {

            sort = ['createdAt']
        }
        var doffset
        var dlimit
        const table1 = req.body.params.tableName;
        const mapperData = await model['TableMapper'].findOne({ where: { view: table1, roleId: userRole } });
        //console.log("function mapper", mapperData);
        // res.send(mapperData);
        if (!req.body.pagination) {
            //default pagination set 
            doffset = 0;
            dlimit = 4;


        } else {
            doffset = po;
            dlimit = pl;
        }

        let filter = {}
        let realtionalfiltter = {}
        if (req.body.filter && req.body.filter.length > 0) {

            req.body.filter.forEach(obj => {
                if (obj.value && !obj.properties.relation) {
                    filter = operatorsForFilters(obj, filter);
                    // else if(obj.value && (obj.inputType =="exclude") ){

                    //     if((obj.value != null) & (obj.value != "")){
                    //         filter[obj.colName] = { [Op.notIn]: obj.value}
                    //     }
                    // }
                }
                else if (obj.value) {
                    if (!realtionalfiltter[obj.tableName]) {
                        realtionalfiltter[obj.tableName] = {}
                    }
                    realtionalfiltter[obj.tableName] = operatorsForFilters(obj, realtionalfiltter[obj.tableName]);
                    //realtionalfiltter[obj.tableName][obj.colName]={[Op.like]: `%` + obj.value + '%'}
                    console.log("realuy8799879-0-=========", realtionalfiltter)

                }
            })

            filter['deletedAt'] = null;
            //console.log("==============>Filter", filter);
        }
        filter = getAllowedFilter(mapperData, filter, req);
        //where for particular table rows.
        if (req.body.params.id) {
            filter[req.body.params["propertyToMap"]] = req.body.params.id
            console.log("filter for trainingId ", filter)
        }

        let relationlString = JSON.parse(JSON.parse(mapperData.relationName));

        // console.log("relationlString =======>",relationlString)
        if (relationlString && relationlString.length > 0) {
            relationlString.forEach(obj => {
                obj['where'] = realtionalfiltter[obj['model']];
                obj['model'] = model[obj['model']];
                if (obj['include']) {
                    obj['include'] = getRelationalObject(obj['include'], realtionalfiltter)
                }

            })

        }
        console.log("==============>realtionalfiltter", realtionalfiltter);
        console.log("==============>relationlString", relationlString);

        let att = await JSON.parse(JSON.parse(mapperData.attributes))

        console.log("=====================", model[mapperData.fromTable])
        const data = await model[mapperData.fromTable].findAndCountAll({

            where: filter, offset: doffset, limit: dlimit,
            ...(relationlString && { include: relationlString }),
            order: sort,
            raw: true,
            attributes: att.columnNames


        });
        data['attributes'] = att
        res.status(200).send({
            message: "get user data succesfully",
            result: {
                data: data
            }
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.stack
        })
    }
}

function getRelationalObject(includeModels, realtionalfiltter) {
    includeModels.forEach(each => {
        each['where'] = realtionalfiltter[each['model']];
        each['model'] = model[each['model']];

    })
    return includeModels
}

function operatorsForFilters(obj, filter) {
    if (obj.value && (obj.inputType == "dateTimeRange" || obj.inputType == "dateRange")) {

        if ((obj.value["startDate"]) && (obj.value["endDate"])) {
            //console.log("dob called ")
            filter[obj.colName] = { [Op.between]: [obj.value["startDate"], obj.value["endDate"]] };
        }
    }
    else if (obj.value && obj.inputType == "sliderRange") {
        if (((obj.value[0] != null) && (obj.value[0] != "")) && ((obj.value[1] != null) && (obj.value[1] != "")) && (obj.properties.isActive)) {

            filter[obj.colName] = { [Op.between]: [obj.value[0], obj.value[1]] };
        }
    }

    else if (obj.value && (obj.inputType == "number" || obj.inputType == "dropdown")) {
        if (obj.properties.operator) {
            filter[obj.colName] = { [Op[obj.properties.operator]]: obj.value };
        }
        else {

            filter[obj.colName] = { [Op.eq]: obj.value };
        }
    }
    else if (obj.value && obj.inputType == "radio") {

        if ((obj.value != null) && (obj.value != "")) {

            filter[obj.colName] = { [Op.eq]: obj.value };
        }

    }
    else if (obj.value && obj.inputType == "boolean") {

        if ((obj.value != null) && (obj.value != "")) {
            let temp = obj.value ? "true" : "false";
            filter[obj.colName] = { [Op.eq]: temp };
        }
    }
    else if (obj.value && obj.inputType == "slider") {
        if (((obj.value != null) && (obj.value != "")) && (obj.properties.isActive)) {
            filter[obj.colName] = { [Op.eq]: obj.value };
        }
    }
    else if (obj.value && obj.inputType == "text") {
        if ((obj.value != null) && (obj.value != "")) {
            filter[obj.colName] = { [Op.like]: `%${obj.value}%` };
        }
    }
    else if (obj.value && (obj.inputType == "date" || obj.inputType == "dateOnly")) {

        if ((obj.value != null) & (obj.value != "")) {
            filter[obj.colName] = { [Op.like]: `%${obj.value}%` };
        }
    }

    return filter
}

function getAllowedFilter(mapperData, filter, req) {
    let role = req.user.role;
    let accessObject = JSON.parse(JSON.parse(mapperData.allowedAccess));
    let access = accessObject[role];
    console.log("things ", accessObject);
    if (access.dataAllowed == "My") {
        filter[access.propertyToMap] = { [Op.eq]: req.headers.id };
    }
    else if (access.dataAllowed == "Others") {
        filter[access.propertyToMap] = { [Op.ne]: req.headers.id };
    }
    return filter
}
