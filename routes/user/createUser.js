const model = require('./../../model/index');
const { Op, Model } = require('sequelize');
const _ = require("lodash");

module.exports = async (req, res) => {
    try {
        const t1 = req.body.params.tableName;
        const userRole = req.user.roleId;
        const userCase = req.body.params.case;
        var id;
        var userId = {};
        var formFieldsObject;
        var formValues;
        const formMapper = await model['FormMapper'].findOne({ where: { view: t1, roleId: userRole } });
        if (userCase == "edit" || userCase == "update") {

            //getting edit fields for relational table
            //console.log("formMapper ",formMapper)
            let relationlString = JSON.parse(JSON.parse(formMapper.relationName));
            // console.log("relationlString =======>",relationlString)
            if (relationlString && relationlString.length > 0) {
                relationlString.forEach(obj => {
                    // obj['where']=realtionalfiltter[obj['model']];
                    console.log("obj=====1 ", obj)
                    obj['model'] = model[obj['model']];
                    if (obj['include']) {
                        obj['include'] = getRelationalObject(obj['include'])
                    }
                })

            }

            console.log("relational ", relationlString)
            formFieldsObject = JSON.parse(JSON.parse(formMapper.editFields));
            id = req.body.params.id;
            userId["id"] = { [Op.eq]: id };
            console.log("----------", {
                where: userId,
                ...(relationlString && { include: relationlString }),
                raw: true
            })
            formValues = await model[formMapper.fromTable].findAndCountAll({
                where: userId,
                ...(relationlString && { include: relationlString }),
                raw: true
            });
            console.log("raw values ", formValues) //.edu_employee.dataValues.user_languages)
            //var mergedObject = _.merge(formValues, [formValues[0], formValues[1]]);
            //const object1 = await mergeRelationalObject(formValues)
            // console.log("raw values ", formValues[0]) //.edu_employee.dataValues.user_languages)

            for (let i = 0; i < formFieldsObject.length; i++) {
                if (formFieldsObject[i].properties.relation) {
                    let currentCol = formFieldsObject[i].tableName + "." + formFieldsObject[i].colName;
                    formFieldsObject[i].value = formValues.rows[0][currentCol];
                }
                else {
                    formFieldsObject[i].value = formValues.rows[0][formFieldsObject[i].colName];
                }
            }
            res.status(200).send({
                message: "user data form succesfully retreived",
                result: {
                    fields: formFieldsObject
                }
            })
        }
        else if (userCase == "create") {
            formFieldsObject = JSON.parse(JSON.parse(formMapper.createFields));
            res.status(200).send({
                message: "user data form succesfully retreived",
                result: {
                    fields: formFieldsObject
                }
            })
        }
        else {
            res.status(406).send({
                message: "invalid case"
            })
        }
    } catch (err) {
        res.status(406).send({
            message: err.stack
        })
    }
}

function getRelationalObject(includeModels) {
    includeModels.forEach(each => {
        //each['where'] = realtionalfiltter[each['model']];
        console.log("obj=====inside 2 ", each)
        each['model'] = model[each['model']];

    })
    return includeModels
}

async function mergeRelationalObject(result) {
    console.log("result type ", typeof result)
    //result.forEach(obj => {


    // for (const [key, value] of Object.entries(result[0])) {
    //     if (key == result[1][key]) {
    //         value.push(result[i][])
    //     }
    //     console.log(`${key}: ${value}`);
    // }
    // Object.keys(obj.toJSON()).forEach(k => {
    //     if (typeof obj[k] === 'object') {
    //         Object.keys(obj[k]).forEach(j => obj[j] = obj[k][j]);
    //     }
    // });
    // });
    return result;
}

//async function 

// config
    // type 
    // limit
    // tablename

// tobeFilled
//new api for data filling


// fields to be sent in function
//      . loop through objects to check for drop down
//      . check toBeFilled: true
//          . check for dataType to be filled
//          . call that type of data
//          . make diff functions for diff types
//              . return fields