const { query, _attributes } = require('../../config/sequelize');
const model = require('./../../model/index');
const { Op, Model } = require('sequelize');
// const includeAttributes = require('./../../model/attributes');
// const FormMapper = require('../../model/formmapper');


module.exports = async (req, res) => {
    try {
        var userId = {};
        let object = {}
        object['defaultVideo'] = req.body.videoId;
        userId["id"] = 1; //get this id from header.
        const successObj = await updateOrCreate(model['edu_employee'],userId,object);
        res.status(200).send({
            message: "insert user data succesfully",
            result: {
                data: successObj
            }
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.stack
        })
    }
}


async function updateOrCreate (model, where, newItem) {
    const item = await model.update(newItem, {where});
    return {item, created: false};
}

// properties [relation:true,foriegnKey:true, colName :"id", tableName:"", disabled:true]