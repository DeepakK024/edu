const { query, _attributes } = require('../../config/sequelize');
const model = require('./../../model/index');
const { Op, Model } = require('sequelize');
const moment = require('moment');
// const includeAttributes = require('./../../model/attributes');
// const FormMapper = require('../../model/formmapper');
// const { applyForView } = require('../../middleware/applyCheck');


module.exports = async (req, res) => {
    try {
        const tableName = req.body.params.tableName;
        const userRole = req.user.roleId;
        var userId = {};
        const formMapper = await model['FormMapper'].findOne({ where: { view: tableName, roleId: userRole } });
        var id;
        if (req.body.params.id) {
            id = req.body.params.id;
        }
        else {
            id = null;
        }



        updateObject = await insertObject(req.body.params.fields);
        userId["id"] = { [Op.eq]: id };
        const successObj = await updateOrCreate(model[formMapper.fromTable], userId, updateObject.mainModel);

        console.log("looping through")
        for (const [key, value] of Object.entries(updateObject.relationalModels)) {
            console.log(`${key}: ${value.id}`);
            userId["id"] = { [Op.eq]: value.id };
            const successObj = await updateOrCreate(model[key], userId, value);
        }
        //loop for relational model
        // userId["id"] = {[Op.eq]: id};
        // const successObj = await updateOrCreate(model[formMapper.fromTable],userId,updateObject.mainModel);

        // table name 
        res.status(200).send({
            message: "insert user data succesfully",
            result: {
                data: successObj
            }
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.stack
        })
    }
}
async function insertObject(values) {
    let object = {}
    let relationalModels = {}
    values.forEach(obj => {
        if (!obj.properties.relation && !obj.properties.foriegnKey) {
            object[obj.colName] = obj.value;
        }
        else if (obj.value) {
            if (!relationalModels[obj.tableName] && !obj.properties.foriegnKey) {
                relationalModels[obj.tableName] = {}
            }
            if (!obj.properties.disabled && !obj.properties.foriegnKey) {
                relationalModels[obj.tableName][obj.colName] = obj.value
            }
            else if (obj.properties.foriegnKey) {
                if (!relationalModels[obj.properties.tableName]) {
                    relationalModels[obj.properties.tableName] = {}
                }
                relationalModels[obj.properties.tableName][obj.properties.colName] = obj.value
            }

        }


    })
    let data = {
        "mainModel": object,
        "relationalModels": relationalModels
    }
    return data;
}

async function updateOrCreate(model, where, newItem) {
    const foundItem = await model.findOne({ where });
    if (!foundItem) {
        let nowTimeStamp = moment(Date.now()).format('YYYY-MM-DD HH:mm:ss');
        //newItem['createdAt'] = nowTimeStamp;
        const item = await model.create(newItem)
        return { item, created: true };
    }
    const item = await model.update(newItem, { where });
    return { item, created: false };
}

// properties [relation:true,foriegnKey:true, colName :"id", tableName:"", disabled:true]