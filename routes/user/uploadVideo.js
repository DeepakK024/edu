const express = require('express');
const router = express.Router();
var model = require('../../model/index');
const app = require("../../app");

// const { Op, Model } = require('sequelize');
const multer = require('multer');

const fs = require('fs');
const path = require('path');
// var router = require('../../public/profileVideos')
const relativePath = path.resolve(__dirname, '../../public/profileVideos');

var videoFile = {
  "video": ""
};

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    dir = `${relativePath}`;
    fs.mkdir(dir, err => {
      if (err && err.code != 'EEXIST') throw err;
    })
    cb(null, `${dir}`)
  },
  filename: (req, file, cb) => {
    console.log("-------", file)
    var ext = file.originalname.substring(file.originalname.lastIndexOf("."));
    videoFile[file.fieldname] = Date.now() + ext;


    cb(null, videoFile[file.fieldname])


  }
});

const upload = multer({ //multer settings
  storage: storage,
  fileFilter: function (req, file, callback) {
    console.log("============", file)
    var ext = path.extname(file.originalname);
    if (ext !== '.mp4' && ext !== '.mpeg') {
      callback({ message: 'Invalid format' })
    }
    callback(null, true)
  }
}).fields([{ name: "video" }]);

// module.exports = (upload , async (req, res) => {
//     console.log("true")
router.post('/', upload, async function (req, res) {
  try {
    console.log("true")
    console.log(req.body)
    req.setTimeout(1800000)
    //   if(videoFile['video'].length > 0){

    let pathToSend = "/profileVideos";
    videoFile['video'] = `${pathToSend}/${videoFile['video']}`;
    console.log(videoFile['video'])
    // videoFile['video'] = videoFile['video'].replace(/\\/g,"/");


    var newmodels = await model['Videos']
    var resolve = await newmodels.create({
      path: `${videoFile['video']}`,
      employeeId: req.body.employeeId,
      title: req.body.title,
      date: req.body.date

    })
    console.log("resolve ", resolve)

    res.send({ statusCode: 200, result: resolve, message: "video uploaded" });

    //   } else{
    //     res.status(406).send({ statusCode: 406, message: "No video selected"});
    //   }
  } catch (error) {
    console.log(error)
    res.status(406).send({ statusCode: 406, message: error.message });
  }
})


function deleteFile(imagePath) {
  fs.unlink(imagePath, (err) => {
    if (err) throw err;
  });
}

module.exports = router;