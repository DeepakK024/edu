var express = require('express');
var router = express.Router();
const User = require('./../../model/user')
const Sequelize = require("../../config/sequelize");
const model = require('./../../model/index');

module.exports = async (req, res) => {
    try {
        // let a = req.body
        // console.log(a.userId);
        // localhost:3000/deleteuser/?id=2

        const id = req.body.params.id;
        const tableName = req.body.params.tableName;

        const mapperData = await model['deletemapper'].findOne({ where: { view: tableName } });
        console.log("attributes ", id, tableName, model[[mapperData.tableName]], mapperData)
        const deluser = await model[mapperData.fromTable].destroy({
            where: {
                // id:a.userId
                id
            }
        });

        console.log("delUser ", deluser)
        res.status(200).send({
            message: "delete user succesfully",
            result: {
                deluser
            }
        })
    }
    catch (err) {
        res.status(406).send({
            message: err.message
        })
    }
}