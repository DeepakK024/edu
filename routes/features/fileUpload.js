var express = require('express');
const { query, _attributes } = require('../../config/sequelize');
const model = require('./../../model/index');
const { Op, Model } = require('sequelize');

const multer = require('multer');
const fs = require('fs');
const AWS = require('aws-sdk');
const path = require('path');
require('dotenv').config();
const { createParams } = require('../../middleware/aws');
const { preSignedURL } = require('../../middleware/signedURL');
// const s3 = require('../../middleware/aws')

module.exports = async (req, res) => {
    try {

        console.log("req ", req)
        //configuring the AWS environment
        AWS.config.update({
            accessKeyId: process.env.ACCESS_KEY_ID,
            secretAccessKey: process.env.SECRET_ACCESS_KEY,
            region: 'us-east-1'
        });

        await method2(req.files.file[0], res, req);

        // var s3 = new AWS.S3();

        // var filePath = "./";//process.env.PATH_;

        // filePath += req.files.file[0]['originalname']
        // console.log("filePath ", filePath)
        // var fileKey = Date.now() + "_" + path.basename(filePath)

        // //configuring parameters
        // var params = await createParams(fileKey, req.files.file[0])

        // s3.upload(params, function (err, data) {
        //     //handle error
        //     if (err) {
        //         console.log("Error", err);
        //     }

        //     //success
        //     if (data) {
        //         console.log("Uploaded in:", data.Location);
        //     }
        // });

        // res.status(200).send({
        //     message: "get user data succesfully",
        //     result: { key: params.Key }
        // })
    }
    catch (error) {
        console.log(error);
        res.status(406).send({
            message: err.stack
        })
    }
}



async function method2(file, res, req) {
    try {
        console.log("path ", file)
        fsFileReadS3(file, req, res);
    }
    catch (e) {
        return e
    }

}

function fsFileReadS3(file, req, res) {
    fs.readFile(file.path.toString(), async function (err, data) {
        try {
            if (err)
                throw err; // Something went wrong!
            var s3bucket = new AWS.S3({ params: { Bucket: process.env.BUCKET } });
            s3CreateBucket(s3bucket, file, data, req, res);
        }
        catch (e) {
            return e;
        }
    });
}

function s3CreateBucket(s3bucket, file, data, req, res) {
    s3bucket.createBucket(async function () {
        try {
            let filePath = "./";
            filePath += file.originalname;
            var params = {
                Key: process.env.PATH_ + Date.now() + "_" + path.basename(filePath),
                Body: data
            };
            s3UploadInsert(s3bucket, params, file, req, res);
        }
        catch (e) {
            return e;
        }
    });
}

function s3UploadInsert(s3bucket, params, file, req, res) {
    s3bucket.upload(params, async function (err, data) {
        try {
            // Whether there is an error or not, delete the temp file
            fs.unlink(file.path, function (err) {
                if (err) {
                    console.error(err);
                }
                console.log('Temp File Delete');
            });

            //console.log("PRINT FILE:", file);
            // signed url 
            const filePreSignedurl = await preSignedURL(params.Key);

            //insert into db
            const dataResult = await insertInto(req, params.Key, file.mimetype);
            console.log("dataResult ", dataResult.dataValues);
            if (err) {
                console.log('ERROR MSG: ', err);
                res.status(500).send(err);
            } else {
                console.log('Successfully uploaded data', data.Location);
                res.status(200).send({
                    message: "file uploaded successfully",
                    result: { key: params.Key, bucketUrl: data.Location, signedURL: filePreSignedurl, fileId: dataResult.dataValues.id }
                });
            }
        }
        catch (e) {
            return e;
        }

    });
}

async function insertInto(req, keyFile, typeFile) {
    try {
        console.log("===filtType=========== ", typeFile)
        var newmodels = await model['edu_master_file']
        var resolve = await newmodels.create({
            path: keyFile,
            type: typeFile,
            title: req.body.title,
            date: req.body.date

        })
        return resolve
    }
    catch (error) {
        console.log("error=========== ", error);
        return error;
    }

}

