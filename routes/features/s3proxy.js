var express = require('express');
var app = express();
var fs = require('fs');
require('dotenv').config();
var AWS = require('aws-sdk');
const { preSignedURL } = require('../../middleware/signedURL')

app.get('/', function (req, res, next) {
    res.send('You did not say the magic word');
});


module.exports = async (req, res) => {
    try {
        // download the file via aws s3 here
        var fileKey = req.body['fileKey'];

        console.log('Trying to download file', fileKey);

        const urlToObject = await preSignedURL(fileKey)
        console.log("the url is ", urlToObject)

        // AWS.config.update(
        //     {
        //         accessKeyId: process.env.ACCESS_KEY_ID,
        //         secretAccessKey: process.env.SECRET_ACCESS_KEY,
        //         region: 'us-east-1'
        //     }
        // );
        // var s3 = new AWS.S3();
        // var options = {
        //     Bucket: process.env.BUCKET,
        //     Key: fileKey
        // };

        // //
        // getImage(s3, fileKey)
        //     .then((img) => {
        //         let image = "<img src='data:image/jpeg;base64," + encode(img.Body) + "'" + "/>";
        //         let startHTML = "<html><body></body>";
        //         let endHTML = "</body></html>";
        //         let html = startHTML + image + endHTML;
        //         res.send(html)
        //     }).catch((e) => {
        //         res.send(e)
        //     })
        //

        // res.attachment(fileKey);
        // var fileStream = s3.getObject(options).createReadStream();
        // // console.log("filestream ", fileStream)
        // fileStream.pipe(res);

        res.status(200).send({
            result: { linkToFile: urlToObject }
        })
    }
    catch (err) {
        console.log(err);
        res.status(406).send({
            message: err.stack
        })
    }
};
async function getImage(s3, fileKey) {
    const data = s3.getObject(
        {
            Bucket: process.env.BUCKET,
            Key: fileKey
        }

    ).promise();
    return data;
}

function encode(data) {
    let buf = Buffer.from(data);
    let base64 = buf.toString('base64');
    return base64
}

// var server = app.listen(3000, function () {
//     var host = server.address().address;
//     var port = server.address().port;
//     console.log('S3 Proxy app listening at http://%s:%s', host, port);
// });


