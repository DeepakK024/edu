const model = require('./../../model/index');
const { Op, Model } = require('sequelize');

require('dotenv').config();

module.exports = async (req, res) => {
    try {
        const result = await insertInto(req, "link");
        res.status(200).send({
            message: "file uploaded successfully",
            result
        })
    }
    catch (err) {
        console.log(err);
        res.status(406).send({
            message: err.stack
        })
    }
}

async function insertInto(req, typeFile) {
    var newmodels = model['edu_master_file']
    var resolve = await newmodels.create({
        path: req.body.link,
        type: typeFile,
        title: req.body.link,
        date: req.body.date
    })
    return resolve
}

