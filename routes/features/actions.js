const model = require('./../../model/index');
const { Op, Model } = require('sequelize');
const { insertAction } = require('../../functions/insert/insertActions');

require('dotenv').config();

module.exports = async (req, res) => {
    try {
        const action = req.body.params.tableName;
        const userRole = req.user.roleId;
        const formMapper = await model['FormMapper'].findOne({ where: { view: action, roleId: userRole } });

        const results = await insertAction(formMapper, req.body.params.data, res, req);

        if (results) {
            res.status(200).send({
                message: "success",
                results
            })
        }
        else {
            throw new Error;
        }


    }
    catch (err) {
        console.log(err);
        res.status(406).send({
            message: "Already Applied"
        })
    }
}