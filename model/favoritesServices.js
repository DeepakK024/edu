'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const FavoriteServices = sequelize.define('FavoriteServices', {
    serviceId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('serviceId')
        },
        set(value) {
            this.setDataValue('serviceId', value);
        }
    },
    employerId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('employerId')
        },
        set(value) {
            this.setDataValue('employerId', value);
        }
    },
    serviceProviderId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('serviceProviderId')
        },
        set(value) {
            this.setDataValue('serviceProviderId', value);
        }
    }
}, {
    tableName: 'edu_favorites_services'


});

console.log("FavoriteServices", FavoriteServices === sequelize.models.FavoriteServices);
module.exports = FavoriteServices;