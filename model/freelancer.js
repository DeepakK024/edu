'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");
const Projects = require('./projectwork');
const Expertise = require('./expertise');
const AppliedTrainings = require('./appliedTrainings');

const Freelancer = sequelize.define('edu_freelancer',{
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    firstName:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('firstName')
      },
      set(value){
          this.setDataValue('firstName',value);
      }
    },
    lastName:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('lastName')
      },
      set(value){
          this.setDataValue('lastName',value);
      }
    },
    middleName:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('middleName')
      },
      set(value){
          this.setDataValue('middleName',value);
      }
    },
    dob:{
        type:DataTypes.DATEONLY,
        allowNull:true,
        get(){
            return this.getDataValue('dob')
        },
        set(value){
            return this.setDataValue('dob',value);
        }
    },
    gender: {
        type: DataTypes.TINYINT,
        allowNull: true,
        get() {
            return this.getDataValue('gender');
        },
        set(value) {
            this.setDataValue('gender', value);
        }
    },
    userId:{
        type:DataTypes.INTEGER,
        allowNull:false,
        get(){
            return this.getDataValue('userId')
        },
        set(value){
            return this.setDataValue('userId',value);
        }
    },
    displayName:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('displayName')
        },
        set(value){
            this.setDataValue('displayName',value);
        }
    },
    },{
      tableName : 'edu_freelancer'
  });

//   Projects.belongsTo(Employee, {
//     foreignKey: 'employeeId'
//   })

//   Expertise.belongsTo(Employee, {
//     foreignKey: 'employeeId'
//   })

  AppliedTrainings.belongsTo(Freelancer, {
    foreignKey: 'freelancerId'
  })
  
  console.log("Freelancer Table",Freelancer === sequelize.models.edu_freelancer); 
  module.exports = Freelancer;