'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const Training = require('./trainings');

const MasterStatus = sequelize.define('master_status', {

    desciption: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('desciption')
        },
        set(value) {
            this.setDataValue('desciption', value);
        }
    }
},
    {
        timestamps: false,
        tableName: 'master_status'
    });

Training.belongsTo(MasterStatus, {
    foreignKey: 'statusId'
})

console.log("MasterStatus Table", MasterStatus === sequelize.models.master_status);
module.exports = MasterStatus;