'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");


const userLanguages = sequelize.define('user_languages', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },

    language: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('language')
        },
        set(value) {
            this.setDataValue('language', value);
        }
    },
    userId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('userId')
        },
        set(value) {
            this.setDataValue('userId', value);
        }
    }
}, {
    tableName: 'user_languages'
});

// Employee.hasMany(userLanguages, {
//     as: 'userId'
// })


console.log("userLanguages Table", userLanguages === sequelize.models.user_languages);
module.exports = userLanguages;