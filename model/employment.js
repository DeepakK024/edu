'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");


const Employments = sequelize.define('Edu_employment',{
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    title:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('title')
      },
      set(value){
          this.setDataValue('title',value);
      }
    },
    employmentType:{
        type:DataTypes.INTEGER,
        get(){
            return this.getDataValue('employmentType')
        },
        set(value){
            this.setDataValue('employmentType',value);
        }
    },
    company:{
    type:DataTypes.STRING,
    get(){
        return this.getDataValue('company')
    },
    set(value){
        this.setDataValue('company',value);
    }
    },
    companyLocation:{
    type:DataTypes.STRING,
    get(){
        return this.getDataValue('companyLocation')
    },
    set(value){
        this.setDataValue('companyLocation',value);
    }
    },
    startYear:{
    type:DataTypes.STRING,
    get(){
        return this.getDataValue('startYear')
    },
    set(value){
        this.setDataValue('startYear',value);
    }
    },
    endYear:{
    type:DataTypes.STRING,
    get(){
        return this.getDataValue('endYear')
    },
    set(value){
        this.setDataValue('endYear',value);
    }
    },
    description:{
    type:DataTypes.STRING,
    get(){
        return this.getDataValue('description')
    },
    set(value){
        this.setDataValue('description',value);
    }
    },
    url:{
    type:DataTypes.STRING,
    get(){
        return this.getDataValue('url')
    },
    set(value){
        this.setDataValue('url',value);
    }
    },
    employeeId:{
        type:DataTypes.INTEGER,
        get(){
            return this.getDataValue('employeeId')
        },
        set(value){
            return this.setDataValue('employeeId',value);
        }
    },
    },{
      tableName : 'edu_employments'
  });


  console.log("Employments Table",Employments === sequelize.models.Edu_employment); 
  module.exports = Employments;