'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");


const Projects = sequelize.define('Edu_projectwork',{
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    title:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('title')
      },
      set(value){
          this.setDataValue('title',value);
      }
    },
    description:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('description')
        },
        set(value){
            this.setDataValue('description',value);
        }
      },
      startDate:{
        type:DataTypes.DATE,
        get(){
            return this.getDataValue('startDate')
        },
        set(value){
            this.setDataValue('startDate',value);
        }
      },
      endDate:{
        type:DataTypes.DATE,
        get(){
            return this.getDataValue('endDate')
        },
        set(value){
            this.setDataValue('endDate',value);
        }
      },
      url:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('url')
        },
        set(value){
            this.setDataValue('url',value);
        }
      },
    employeeId:{
        type:DataTypes.INTEGER,
        get(){
            return this.getDataValue('employeeId')
        },
        set(value){
            return this.setDataValue('employeeId',value);
        }
        },
    },{
      tableName : 'edu_projectworks'
  });


  console.log("Projects Table",Projects === sequelize.models.Edu_projectwork); 
  module.exports = Projects;