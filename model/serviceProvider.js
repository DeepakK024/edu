'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const AppliedTrainings = require('./appliedTrainings');
const Expertise = require('./expertise');
const AppliedServices = require('./appliedServices');
const FavoriteServices = require('./favoritesServices');
const FavoriteJobs = require('./favoritesJobs');

const serviceProvider = sequelize.define('edu_service_provider', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    firstName: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('firstName')
        },
        set(value) {
            this.setDataValue('firstName', value);
        }
    },
    lastName: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('lastName')
        },
        set(value) {
            this.setDataValue('lastName', value);
        }
    },
    middleName: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('middleName')
        },
        set(value) {
            this.setDataValue('middleName', value);
        }
    },
    dob: {
        type: DataTypes.DATEONLY,
        allowNull: true,
        get() {
            return this.getDataValue('dob')
        },
        set(value) {
            return this.setDataValue('dob', value);
        }
    },
    gender: {
        type: DataTypes.TINYINT,
        allowNull: true,
        get() {
            return this.getDataValue('gender');
        },
        set(value) {
            this.setDataValue('gender', value);
        }
    },
    userId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        get() {
            return this.getDataValue('userId')
        },
        set(value) {
            return this.setDataValue('userId', value);
        }
    },
    displayName: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('displayName')
        },
        set(value) {
            this.setDataValue('displayName', value);
        }
    },
}, {
    tableName: 'edu_service_provider'
});

//   Projects.belongsTo(Employee, {
//     foreignKey: 'employeeId'
//   })

Expertise.belongsTo(serviceProvider, {
    foreignKey: 'serviceProviderId'
})

AppliedTrainings.belongsTo(serviceProvider, {
    foreignKey: 'serviceProviderId'
})

AppliedServices.belongsTo(serviceProvider, {
    foreignKey: 'serviceProviderId'
})

FavoriteServices.belongsTo(serviceProvider, {
    foreignKey: 'serviceProviderId'
})

console.log("serviceProvider Table", serviceProvider === sequelize.models.edu_service_provider);
module.exports = serviceProvider;