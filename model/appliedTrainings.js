'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const AppliedTrainings = sequelize.define('AppliedTrainings', {
    trainingId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('trainingId')
        },
        set(value) {
            this.setDataValue('trainingId', value);
        }
    },
    hostingEmployeeId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('hostingEmployeeId')
        },
        set(value) {
            this.setDataValue('hostingEmployeeId', value);
        }
    },
    participantEmployeeId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('participantEmployeeId')
        },
        set(value) {
            this.setDataValue('participantEmployeeId', value);
        }
    },
    serviceProviderId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('serviceProviderId')
        },
        set(value) {
            this.setDataValue('serviceProviderId', value);
        }
    }
}, {
    tableName: 'edu_applied_trainings'


});

console.log("AppliedTrainings", AppliedTrainings === sequelize.models.AppliedTrainings);
module.exports = AppliedTrainings;