'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const AppliedJobs = require('./appliedJobs');
const FavoriteJobs = require('./favoritesJobs');
const Expertise = require('./expertise');

const Jobs = sequelize.define('Edu_job', {
  // id:{
  //   type:DataTypes.INTEGER,
  //   autoIncrement:true,
  //   primaryKey:true
  // },
  jobTitle: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('jobTitle')
    },
    set(value) {
      this.setDataValue('jobTitle', value);
    }
  },
  datePostingOpen: {
    type: DataTypes.DATE,
    get() {
      return this.getDataValue('datePostingOpen')
    },
    set(value) {
      return this.setDataValue('datePostingOpen', value);
    }
  },
  datePostingClose: {
    type: DataTypes.DATE,
    get() {
      return this.getDataValue('datePostingClose')
    },
    set(value) {
      return this.setDataValue('datePostingClose', value);
    }
  },
  websiteUrl: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('websiteUrl')
    },
    set(value) {
      return this.setDataValue('websiteUrl', value);
    }
  },
  jobDescription: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('jobDescription')
    },
    set(value) {
      return this.setDataValue('jobDescription', value);
    }
  },
  minimumQualification: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('minimumQualification')
    },
    set(value) {
      return this.setDataValue('minimumQualification', value);
    }
  },
  payRate: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('payRate');
    },
    set(value) {
      return this.setDataValue('payRate', value);
    }
  },
  postQuestions: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('postQuestions')
    },
    set(value) {
      return this.setDataValue('postQuestions', value);
    }
  },
  employerId: {
    type: DataTypes.INTEGER,
    get() {
      return this.getDataValue('employerId')
    },
    set(value) {
      return this.setDataValue('employerId', value);
    }
  },
}, {
  tableName: 'edu_jobs'
});

AppliedJobs.belongsTo(Jobs, {
  foreignKey: 'jobId'
})

FavoriteJobs.belongsTo(Jobs, {
  foreignKey: 'jobId'
})

Expertise.belongsTo(Jobs, {
  foreignKey: 'jobId'
})

console.log("Jobs Table", Jobs === sequelize.models.Edu_job);
module.exports = Jobs;