'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const FavoriteJobs = sequelize.define('FavoriteJobs', {
    jobId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('jobId')
        },
        set(value) {
            this.setDataValue('jobId', value);
        }
    },
    employerId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('employerId')
        },
        set(value) {
            this.setDataValue('employerId', value);
        }
    },
    employeeId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('employeeId')
        },
        set(value) {
            this.setDataValue('employeeId', value);
        }
    }
}, {
    tableName: 'edu_favorites_jobs'


});

console.log("FavoriteJobs", FavoriteJobs === sequelize.models.FavoriteJobs);
module.exports = FavoriteJobs;