'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");

const masterPrivilige = sequelize.define('master_priviliges', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },

    priviliges: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('priviliges')
        },
        set(value) {
            this.setDataValue('priviliges', value);
        }
    }
},
    {
        tableName: "priviliges"
    });

console.log("masterPrivilige Table", masterPrivilige === sequelize.models.master_priviliges);
module.exports = masterPrivilige;