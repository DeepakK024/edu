'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");


const Education = sequelize.define('Edu_education',{
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    institueName:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('institueName')
      },
      set(value){
          this.setDataValue('institueName',value);
      }
    },
    degree:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('degree')
      },
      set(value){
          return this.setDataValue('degree',value);
      }
    },
    field:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('field')
        },
        set(value){
            return this.setDataValue('field',value);
        }
      },
      institueLocation:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('institueLocation')
        },
        set(value){
            return this.setDataValue('institueLocation',value);
        }
      },
      startYear:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('startYear')
        },
        set(value){
            return this.setDataValue('startYear',value);
        }
      },
      endYear:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('endYear')
        },
        set(value){
            return this.setDataValue('endYear',value);
        }
      },
      grade:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('grade')
        },
        set(value){
            return this.setDataValue('grade',value);
        }
      },
      description:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('description')
        },
        set(value){
            return this.setDataValue('description',value);
        }
      },
      url:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('url')
        },
        set(value){
            return this.setDataValue('url',value);
        }
      },
    },{
      tableName : 'edu_educations'
  });


  console.log("Education Table",Education === sequelize.models.Edu_education); 
  module.exports = Education;