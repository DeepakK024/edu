'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
//const AppliedJobs = require('../model/appliedJobs')
const Employee = require('./employee');

const Videos = sequelize.define('edu_video', {
  // id:{
  //   type:DataTypes.INTEGER,
  //   autoIncrement:true,
  //   primaryKey:true
  // },

  fileId: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('fileId')
    },
    set(value) {
      this.setDataValue('fileId', value);
    }
  },
  employeeId: {
    type: DataTypes.INTEGER,
    get() {
      return this.getDataValue('employeeId')
    },
    set(value) {
      this.setDataValue('employeeId', value);
    }
  }
},
  {
    timestamps: false,

    tableName: "edu_videos"
  });

// Employee.belongsTo(Videos, {
//   foreignKey: 'defaultVideo'
// })

console.log("video Table", Videos === sequelize.models.edu_video);
module.exports = Videos;