'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const AppliedJobs = sequelize.define('AppliedJobs', {
    jobId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('jobId')
        },
        set(value) {
            this.setDataValue('jobId', value);
        }
    },
    employerId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('employerId')
        },
        set(value) {
            this.setDataValue('employerId', value);
        }
    },
    employeeId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('employeeId')
        },
        set(value) {
            this.setDataValue('employeeId', value);
        }
    },
    fileId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('fileId')
        },
        set(value) {
            this.setDataValue('fileId', value);
        }
    },
    appliedDate: {
        type: DataTypes.DATE,
        get() {
            return this.getDataValue('appliedDate')
        },
        set(value) {
            this.setDataValue('appliedDate', value);
        }
    }
}, {
    tableName: 'edu_applied_jobs'


});

console.log("AppliedJobs", AppliedJobs === sequelize.models.AppliedJobs);
module.exports = AppliedJobs;