'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");


const Expertise = sequelize.define('Edu_expertise', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    masterExpertiseId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('masterExpertiseId')
        },
        set(value) {
            this.setDataValue('masterExpertiseId', value);
        }
    },
    employeeId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('employeeId')
        },
        set(value) {
            return this.setDataValue('employeeId', value);
        }
    },
    serviceProviderId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('serviceProviderId')
        },
        set(value) {
            return this.setDataValue('serviceProviderId', value);
        }
    },
    jobId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('jobId')
        },
        set(value) {
            return this.setDataValue('jobId', value);
        }
    },
    serviceId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('serviceId')
        },
        set(value) {
            return this.setDataValue('serviceId', value);
        }
    }
}, {
    tableName: 'edu_expertises'
});


console.log("Expertise Table", Expertise === sequelize.models.Edu_expertise);
module.exports = Expertise;