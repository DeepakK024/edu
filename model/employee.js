'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const Projects = require('./projectwork');
const Expertise = require('./expertise');
const Employments = require('./employment');
const Certifications = require('./certifications');
const Education = require('./education');
const Videos = require('./video');
const Training = require('./trainings');
const AppliedJobs = require('./appliedJobs');
const AppliedTrainings = require('./appliedTrainings');
const userLanguages = require('./userLanguages');
const FavoriteJobs = require('./favoritesJobs');

const Employee = sequelize.define('edu_employee', {
  // id:{
  //   type:DataTypes.INTEGER,
  //   autoIncrement:true,
  //   primaryKey:true
  // },
  firstName: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('firstName')
    },
    set(value) {
      this.setDataValue('firstName', value);
    }
  },
  lastName: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('lastName')
    },
    set(value) {
      this.setDataValue('lastName', value);
    }
  },
  middleName: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('middleName')
    },
    set(value) {
      this.setDataValue('middleName', value);
    }
  },

  country: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('country')
    },
    set(value) {
      this.setDataValue('country', value);
    }
  },
  state: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('state')
    },
    set(value) {
      this.setDataValue('state', value);
    }
  },
  city: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('city')
    },
    set(value) {
      this.setDataValue('city', value);
    }
  },
  address: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('address')
    },
    set(value) {
      this.setDataValue('address', value);
    }
  },
  about: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('about')
    },
    set(value) {
      this.setDataValue('about', value);
    }
  },
  dob: {
    type: DataTypes.DATEONLY,
    allowNull: true,
    get() {
      return this.getDataValue('dob')
    },
    set(value) {
      return this.setDataValue('dob', value);
    }
  },
  gender: {
    type: DataTypes.TINYINT,
    allowNull: true,
    get() {
      return this.getDataValue('gender');
    },
    set(value) {
      this.setDataValue('gender', value);
    }
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    get() {
      return this.getDataValue('userId')
    },
    set(value) {
      return this.setDataValue('userId', value);
    }
  },
  defaultVideo: {
    type: DataTypes.INTEGER,
    get() {
      return this.getDataValue('defaultVideo')
    },
    set(value) {
      return this.setDataValue('defaultVideo', value);
    }
  },
  displayName: {
    type: DataTypes.STRING,
    get() {
      return this.getDataValue('displayName')
    },
    set(value) {
      this.setDataValue('displayName', value);
    }
  },
  resume: {
    type: DataTypes.INTEGER,
    get() {
      return this.getDataValue('resume')
    },
    set(value) {
      this.setDataValue('resume', value);
    }
  }
}, {
  tableName: 'edu_employee'
});

Projects.belongsTo(Employee, {
  foreignKey: 'employeeId'
})

Expertise.belongsTo(Employee, {
  foreignKey: 'employeeId'
})

Employments.belongsTo(Employee, {
  foreignKey: 'employeeId'
})

Certifications.belongsTo(Employee, {
  foreignKey: 'employeeId'
})

Education.belongsTo(Employee, {
  foreignKey: 'employeeId'
})

Videos.belongsTo(Employee, {
  foreignKey: 'employeeId'
})
Training.belongsTo(Employee, {
  foreignKey: 'employeeId'
})

AppliedJobs.belongsTo(Employee, {
  foreignKey: 'employeeId'
})

AppliedTrainings.belongsTo(Employee, {
  foreignKey: 'hostingEmployeeId'
})

AppliedTrainings.belongsTo(Employee, {
  foreignKey: 'participantEmployeeId'
})

// Employee.hasMany(userLanguages, {
//   foreignKey: 'userId',
//   sourceKey: 'id'
// })

userLanguages.belongsTo(Employee, {
  foreignKey: 'userId'
})

FavoriteJobs.belongsTo(Employee, {
  foreignKey: 'employeeId'
})


console.log("Employee Table", Employee === sequelize.models.edu_employee);
module.exports = Employee;