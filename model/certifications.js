'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");


const Certifications = sequelize.define('Edu_certification',{
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    name:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('name')
      },
      set(value){
          this.setDataValue('name',value);
      }
    },
    issuingOrganization:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('issuingOrganization')
        },
        set(value){
            return this.setDataValue('issuingOrganization',value);
        }
    },
    issueDate:{
      type:DataTypes.DATE,
      get(){
          return this.getDataValue('issueDate')
      },
      set(value){
          return this.setDataValue('issueDate',value);
      }
    },
    expiryDate:{
        type:DataTypes.DATE,
        get(){
            return this.getDataValue('expiryDate')
        },
        set(value){
            return this.setDataValue('expiryDate',value);
        }
      },
    },{
      tableName : 'edu_certifications'
  });


  console.log("Certification Table ",Certifications === sequelize.models.Edu_certification); 
  module.exports = Certifications;