'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const Videos = require('./video')
const AppliedJobs = require('./appliedJobs');
const User = require('./user');
const Employee = require('./employee');

const Files = sequelize.define('edu_master_file', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },

    path: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('path')
        },
        set(value) {
            this.setDataValue('path', value);
        }
    },
    type: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('type')
        },
        set(value) {
            this.setDataValue('type', value);
        }
    },
    title: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('title')
        },
        set(value) {
            this.setDataValue('title', value);
        }
    },
    date: {
        type: DataTypes.DATE,
        get() {
            return this.getDataValue('date')
        },
        set(value) {
            this.setDataValue('date', value);
        }
    }

},
    {
        timestamps: false,

        tableName: "edu_master_files"
    });
Videos.belongsTo(Files, {
    foreignKey: 'fileId'
})

AppliedJobs.belongsTo(Files, {
    foreignKey: 'fileId'
})

User.belongsTo(Files, {
    foreignKey: 'photo'
})

Employee.belongsTo(Files, {
    foreignKey: 'resume'
})

console.log("Files Table", Files === sequelize.models.edu_master_file);
module.exports = Files;