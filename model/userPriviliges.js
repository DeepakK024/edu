'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");

const userPriviliges = sequelize.define('user_priviliges', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },

    priviligesId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('priviligesId')
        },
        set(value) {
            this.setDataValue('priviligesId', value);
        }
    },
    roleId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('roleId')
        },
        set(value) {
            this.setDataValue('roleId', value);
        }
    }
},
    {
        tableName: "role_priviliges"
    });

console.log("userPriviliges Table", userPriviliges === sequelize.models.user_priviliges);
module.exports = userPriviliges;