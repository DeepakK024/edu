'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const Expertise = require('./expertise');
const AppliedServices = require('./appliedServices');
const FavoriteServices = require('./favoritesServices');

const Services = sequelize.define('Edu_services', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    title: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('title')
        },
        set(value) {
            this.setDataValue('title', value);
        }
    },
    datePostingOpen: {
        type: DataTypes.DATE,
        get() {
            return this.getDataValue('datePostingOpen')
        },
        set(value) {
            return this.setDataValue('datePostingOpen', value);
        }
    },
    datePostingClose: {
        type: DataTypes.DATE,
        get() {
            return this.getDataValue('datePostingClose')
        },
        set(value) {
            return this.setDataValue('datePostingClose', value);
        }
    },
    websiteUrl: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('websiteUrl')
        },
        set(value) {
            return this.setDataValue('websiteUrl', value);
        }
    },
    description: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('jobDescription')
        },
        set(value) {
            return this.setDataValue('jobDescription', value);
        }
    },
    budget: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('budget')
        },
        set(value) {
            return this.setDataValue('budget', value);
        }
    },
    employerId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('employerId')
        },
        set(value) {
            return this.setDataValue('employerId', value);
        }
    },
}, {
    tableName: 'edu_services'
});

Expertise.belongsTo(Services, {
    foreignKey: 'serviceId'
})

AppliedServices.belongsTo(Services, {
    foreignKey: 'serviceId'
})

FavoriteServices.belongsTo(Services, {
    foreignKey: 'serviceId'
})

console.log("Services Table", Services === sequelize.models.Edu_services);
module.exports = Services;