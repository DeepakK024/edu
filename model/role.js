'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const User = require('./user');

const Role = sequelize.define('role', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },

    role: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('role')
        },
        set(value) {
            this.setDataValue('role', value);
        }
    }
},
    {
        tableName: "role"
    });

User.belongsTo(Role, {
    foreignKey: 'roleId'
})

console.log("Role Table", Role === sequelize.models.role);
module.exports = Role;