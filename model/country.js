'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");


const Country = sequelize.define('Country',{
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },
    institueName:{
      type:DataTypes.STRING,
      get(){
          return this.getDataValue('name')
      },
      set(value){
          this.setDataValue('name',value);
      }
    }
    },{
      tableName : 'country',
      paranoid : true
  });


  console.log("Country Table",Country === sequelize.models.Country); 
  module.exports = Country;