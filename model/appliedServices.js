'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const AppliedServices = sequelize.define('AppliedServices', {
    serviceId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('serviceId')
        },
        set(value) {
            this.setDataValue('serviceId', value);
        }
    },
    employerId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('employerId')
        },
        set(value) {
            this.setDataValue('employerId', value);
        }
    },
    serviceProviderId: {
        type: DataTypes.INTEGER,
        get() {
            return this.getDataValue('serviceProviderId')
        },
        set(value) {
            this.setDataValue('serviceProviderId', value);
        }
    },
    appliedDate: {
        type: DataTypes.DATE,
        get() {
            return this.getDataValue('appliedDate')
        },
        set(value) {
            this.setDataValue('appliedDate', value);
        }
    }
}, {
    tableName: 'edu_applied_services'


});

console.log("AppliedServices", AppliedServices === sequelize.models.AppliedServices);
module.exports = AppliedServices;