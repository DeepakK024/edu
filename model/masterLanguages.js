'use strict'
const { DataTypes } = require('sequelize');
const sequelize = require("../config/sequelize");
const userLanguages = require('./userLanguages');


const masterLanguages = sequelize.define('master_languages', {
    // id:{
    //   type:DataTypes.INTEGER,
    //   autoIncrement:true,
    //   primaryKey:true
    // },

    language: {
        type: DataTypes.STRING,
        get() {
            return this.getDataValue('language')
        },
        set(value) {
            this.setDataValue('language', value);
        }
    }
}, {
    tableName: 'master_languages'
});

// userLanguages.belongsTo(masterLanguages, {
//     foreignKey: 'masterLanguageId'
// })

console.log("masterLanguages Table", masterLanguages === sequelize.models.master_languages);
module.exports = masterLanguages;