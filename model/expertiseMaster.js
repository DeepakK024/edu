'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");
const Expertise = require('./expertise')

const MasterExpertise = sequelize.define('MasterExpertise',{
    
      name:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('name')
        },
        set(value){
            this.setDataValue('name',value);
        }
      }
},
{
    timestamps: false,
    tableName : 'master_expertises'
});

Expertise.belongsTo(MasterExpertise, {
    foreignKey: 'masterExpertiseId'
})
console.log("MasterExpertise Table",MasterExpertise === sequelize.models.MasterExpertise); 
module.exports = MasterExpertise;