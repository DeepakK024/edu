const User = require('./user');
const TableMapper = require('./tablemapper')
const FilterMapper = require('./filtermapper')
const FormMapper = require('./formmapper')
const edu_employee = require('./employee')
const edu_jobs = require('./Jobs')
const edu_education = require('./education')
const edu_certification = require('./certifications')
const Edu_Employer = require('./employer')
const edu_employment = require('./employment')
const edu_projectwork = require('./projectwork')
const edu_expertise = require('./expertise')
const country = require('./country')
const deletemapper = require('./deletemapper')
const edu_training = require('./trainings')
const edu_video = require('./video')
const master_expertise = require('./expertiseMaster')
const edu_applied_job = require('./appliedJobs')
const edu_applied_training = require('./appliedTrainings')
const edu_service_provider = require('./serviceProvider')
const master_status = require('./masterStatus');
const edu_master_file = require('./file');
const role = require('./role');
const master_languages = require('./masterLanguages');
const user_languages = require('./userLanguages');
const user_session = require('./userAuth');
const master_priviliges = require('./masterPriviliges');
const user_priviliges = require('./userPriviliges');
const edu_favorites_jobs = require('./favoritesJobs');
const edu_services = require('./services');
const edu_applied_service = require('./appliedServices');
const edu_favorites_services = require('./favoritesServices');


const models = {
    User,
    TableMapper,
    FilterMapper,
    FormMapper,
    edu_employee,
    edu_jobs,
    edu_education,
    edu_certification,
    Edu_Employer,
    edu_employment,
    edu_projectwork,
    edu_expertise,
    country,
    deletemapper,
    edu_training,
    edu_video,
    master_expertise,
    edu_applied_job,
    edu_applied_training,
    edu_service_provider,
    master_status,
    edu_master_file,
    role,
    master_languages,
    user_languages,
    user_session,
    master_priviliges,
    user_priviliges,
    edu_favorites_jobs,
    edu_services,
    edu_applied_service,
    edu_favorites_services

}
module.exports = models