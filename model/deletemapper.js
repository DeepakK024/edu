'use strict'
const {DataTypes} = require('sequelize');
const sequelize = require("../config/sequelize");

const DeleteMapper = sequelize.define('DeleteMapper',{
    
      view:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('view')
        },
        set(value){
            this.setDataValue('view',value);
        }
      },
      fromTable:{
        type:DataTypes.STRING,
        get(){
            return this.getDataValue('fromTable')
        },
        set(value){
            this.setDataValue('fromTable',value);
        }
      }
},
{
    timestamps: false,
    tableName : 'deletemapper'
});
console.log("DeleteMapper Table",DeleteMapper === sequelize.models.DeleteMapper); 
module.exports = DeleteMapper;