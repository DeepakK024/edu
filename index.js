// [{
//     "colName": "id",
//     "label": "ID",
//     "tableName": "Member",
//     "inputType": "number",
//     "value": null,
//     "properties": {

//         "string": true
//     }
// },
// {
//     "colName": "fName",
//     "label": "First Name",
//     "tableName": "Member",
//     "inputType": "text",
//     "value": null,
//     "properties": {

//         "string": true
//     }
// },
// {
//     "colName": "mName",
//     "label": "Middle Name",
//     "tableName": "Member",
//     "inputType": "text",
//     "value": null,
//     "properties": {

//         "string": true
//     }
// },
// {
//     "colName": "LName",
//     "label": "Last Name",
//     "tableName": "Member",
//     "inputType": "text",
//     "value": null,
//     "properties": {

//         "string": true
//     }
// },
// {
//     "colName": "phoneNumber",
//     "label": "Phone Number",
//     "tableName": "Member",
//     "inputType": "text",
//     "value": null,
//     "properties": {

//         "string": true
//     }
// },
// {
//     "colName": "licenseNumber",
//     "label": "License Number",
//     "tableName": "Member",
//     "inputType": "number",
//     "value": null,
//     "properties": {

//         "string": true
//     }
// },
// {
//     "colName": "name",
//     "label": "Agent Name",
//     "tableName": "Agent",
//     "inputType": "text",
//     "value": null,
//     "properties": {

//         "string": true,
//         "relation": true
//     }
// },
// {
//     "colName": "gender",
//     "label": "Gender",
//     "tableName": "Member",
//     "inputType": "dropdown",
//     "value": null,
//     "properties": {

//         "string": true,
//         "id": true,
//         "relation": false,
//         "data": [{
//                 "id": 1,
//                 "name": "male"
//             },
//             {
//                 "id": 2,
//                 "name": "female"
//             }
//         ],
//         "propertyToBind": "id",
//         "propertyToShow": "name"
//     }
// },
// {
//     "colName": "dob",
//     "label": "Date Of Birth",
//     "tableName": "Member",
//     "inputType": "DATEONLY",
//     "value": null,
//     "properties": {
//         "dateTime": false
//     }
// },


// {
//     "colName": "dob",
//     "label": "Date Range",
//     "tableName": "Member",
//     "inputType": "dateRange",
//     "value": {
//         "startDate": null,
//         "endDate": null
//     },
//     "properties": {
//         "string": true,
//         "relation": false
//     }
// },

// {
//     "colName": "dob",
//     "label": "Date Time",
//     "tableName": "Member",
//     "inputType": "dateTime",
//     "value": null,
//     "properties": {
//         "dateTime": true
//     }
// },

// {
//     "colName": "dob",
//     "label": "Time Range",
//     "tableName": "Member",
//     "inputType": "dateTimeRange",
//     "value": {
//         "startDate": null,
//         "endDate": null
//     },
//     "properties": {
//         "dateTime": true,
//         "relation": false
//     }
// },

// {
//     "colName": "salisPoint",
//     "label": "Slider",
//     "tableName": "Member",
//     "inputType": "slider",
//     "value": 0,
//     "properties": {
//         "id": true,
//         "isActive": false,
//         "config": {
//             "connect": false,
//             "margin": 1,
//             "range": {
//                 "min": 0,
//                 "max": 50
//             },
//             "step": 1,
//             "start":0
//         }
//     }
// },

// {
//     "colName": "salisPoint",
//     "label": "Slider Range",
//     "tableName": "Member",
//     "inputType": "sliderRange",
//     "value": [0, 50],
//     "properties": {
//         "id": true,
//         "relation": false,
//         "isActive": false,
//         "config": {
//             "connect": true,
//             "margin": 1,
//             "range": {
//                 "min": 0,
//                 "max": 50
//             },
//             "start": [0, 50],
//             "step":1
//         }
//     }
// },


// {
//     "colName": "status",
//     "label": "Status",
//     "tableName": "Member",
//     "inputType": "boolean",
//     "value": null,
//     "properties": {

//     }
// }
// ]


[{
    "colName": "id",
    "label": "ID",
    "tableName": "Member",
    "inputType": "number",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "fName",
    "label": "First Name",
    "tableName": "Member",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "mName",
    "label": "Middle Name",
    "tableName": "Member",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "LName",
    "label": "Last Name",
    "tableName": "Member",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "phoneNumber",
    "label": "Phone Number",
    "tableName": "Member",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "licenseNumber",
    "label": "License Number",
    "tableName": "Member",
    "inputType": "number",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "name",
    "label": "Agent Name",
    "tableName": "Agent",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true,
        "relation": true
    }
},
{
    "colName": "gender",
    "label": "Gender",
    "tableName": "Member",
    "inputType": "dropdown",
    "value": null,
    "properties": {

        "string": true,
        "id": true,
        "relation": false,
        "data": [{
                "id": 1,
                "name": "male"
            },
            {
                "id": 2,
                "name": "female"
            }
        ],
        "propertyToBind": "id",
        "propertyToShow": "name"
    }
},
{
    "colName": "dob",
    "label": "Date Of Birth",
    "tableName": "Member",
    "inputType": "dateOnly",
    "value": null,
    "properties": {
        "dateTime": false
    }
},


{
    "colName": "dob",
    "label": "Date Range",
    "tableName": "Member",
    "inputType": "dateRange",
    "value": {
        "startDate": null,
        "endDate": null
    },
    "properties": {
        "string": true,
        "relation": false
    }
},

{
    "colName": "dob",
    "label": "Date Time",
    "tableName": "Member",
    "inputType": "dateTime",
    "value": null,
    "properties": {
        "dateTime": true
    }
},

{
    "colName": "dob",
    "label": "Time Range",
    "tableName": "Member",
    "inputType": "dateTimeRange",
    "value": {
        "startDate": null,
        "endDate": null
    },
    "properties": {
        "dateTime": true,
        "relation": false
    }
},

{
    "colName": "salisPoint",
    "label": "Slider",
    "tableName": "Member",
    "inputType": "slider",
    "value": 0,
    "properties": {
        "id": true,
        "isActive": false,
        "config": {
            "connect": false,
            "margin": 1,
            "range": {
                "min": 0,
                "max": 50
            },
            "step": 1,
            "start":0
        }
    }
},

{
    "colName": "salisPoint",
    "label": "Slider Range",
    "tableName": "Member",
    "inputType": "sliderRange",
    "value": [0, 50],
    "properties": {
        "id": true,
        "relation": false,
        "isActive": false,
        "config": {
            "connect": true,
            "margin": 1,
            "range": {
                "min": 0,
                "max": 50
            },
            "start": [0, 50],
            "step":1
        }
    }
},


{
    "columnNames":[
       "id",
       "fName",
       "mName",
       "lName",
       "phoneNumber",
       "licenseNumber",
       "salisPoint",
       "feedback",
       "dob",
       "gender",
       "status"
    ],
    "attributes":[
       {
          "colName":"id",
          "label":"ID",
          "type":"text"
       },
       {
          "colName":"fName",
          "label":"First Name",
          "type":"text"
       },
       {
          "colName":"mName",
          "label":"Middle Name",
          "type":"text"
       },
       {
          "colName":"lName",
          "label":"Last Name",
          "type":"text"
       },
       {
          "colName":"phoneNumber",
          "label":"Phone Number",
          "type":"text"
       },
       {
          "colName":"licenseNumber",
          "label":"License Number",
          "type":"text"
       },
       {
          "colName":"",
          "label":"Agent",
          "type":"group",
          "properties":[
             "Agent.name",
             "Agent.email"
          ]
       },
        {
          "colName":"feedback",
          "label":"feedback",
          "type":"text"
       },
       {
          "colName":"dob",
          "label":"Date Of Birth",
          "type":"date"
       },
       {
        "colName":"gender",
        "label":"Gender",
        "type":"enum",
        "properties":{
            "1":"Male",
            "2":"Female"
        }
     },
       {
          "colName":"salisPoint",
          "label":"Salis Point",
          "type":"number"
       },
       {
          "colName":"action",
          "label":"Action",
          "type":"action",
          "allowedAction":{
             "Edit":false,
             "delete":false,
             "view":true,
             "status":true
          }
       }
    ]
 }


 ///////////////

 "{\n   \"columnNames\":[\n      \"id\",\n      \"fName\",\n      \"mName\",\n      \"lName\",\n      \"phoneNumber\",\n      \"licenseNumber\",\n      \"salisPoint\",\n      \"feedback\",\n      \"dob\",\n      \"status\"\n   ],\n   \"attributes\":[\n      {\n         \"colName\":\"id\",\n         \"label\":\"ID\",\n         \"type\":\"text\"\n      },\n      {\n         \"colName\":\"fName\",\n         \"label\":\"First Name\",\n         \"type\":\"text\"\n      },\n      {\n         \"colName\":\"mName\",\n         \"label\":\"Middle Name\",\n         \"type\":\"text\"\n      },\n      {\n         \"colName\":\"lName\",\n         \"label\":\"Last Name\",\n         \"type\":\"text\"\n      },\n      {\n         \"colName\":\"phoneNumber\",\n         \"label\":\"Phone Number\",\n         \"type\":\"text\"\n      },\n      {\n         \"colName\":\"licenseNumber\",\n         \"label\":\"License Number\",\n         \"type\":\"text\"\n      },\n      {\n         \"colName\":\"\",\n         \"label\":\"Agent\",\n         \"type\":\"group\",\n         \"properties\":[\n            \"Agent.name\",\n            \"Agent.email\"\n         ]\n      },\n       {\n         \"colName\":\"feedback\",\n         \"label\":\"feedback\",\n         \"type\":\"text\"\n      },\n      {\n         \"colName\":\"dob\",\n         \"label\":\"Date Of Birth\",\n         \"type\":\"date\"\n      },\n      {\n         \"colName\":\"salisPoint\",\n         \"label\":\"Salis Point\",\n         \"type\":\"number\"\n      },\n      {\n         \"colName\":\"action\",\n         \"label\":\"Action\",\n         \"type\":\"action\",\n         \"allowedAction\":{\n            \"Edit\":false,\n            \"delete\":false,\n            \"view\":true,\n            \"status\":true\n         }\n      }\n   ]\n}"

 

[{
    "colName": "id",
    "label": "ID",
    "tableName": "Member",
    "inputType": "number",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "fName",
    "label": "First Name",
    "tableName": "Member",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "mName",
    "label": "Middle Name",
    "tableName": "Member",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "LName",
    "label": "Last Name",
    "tableName": "Member",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "phoneNumber",
    "label": "Phone Number",
    "tableName": "Member",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "licenseNumber",
    "label": "License Number",
    "tableName": "Member",
    "inputType": "number",
    "value": null,
    "properties": {

        "string": true
    }
},
{
    "colName": "name",
    "label": "Agent Name",
    "tableName": "Agent",
    "inputType": "text",
    "value": null,
    "properties": {

        "string": true,
        "relation": true
    }
},
{
    "colName": "gender",
    "label": "Gender",
    "tableName": "Member",
    "inputType": "dropdown",
    "value": null,
    "properties": {

        "string": true,
        "id": true,
        "relation": false,
        "data": [{
                "id": 1,
                "name": "male"
            },
            {
                "id": 2,
                "name": "female"
            }
        ],
        "propertyToBind": "id",
        "propertyToShow": "name"
    }
},
{
    "colName": "dob",
    "label": "Date Of Birth",
    "tableName": "Member",
    "inputType": "dateOnly",
    "value": null,
    "properties": {
        "dateTime": false
    }
},


{
    "colName": "dob",
    "label": "Date Range",
    "tableName": "Member",
    "inputType": "dateRange",
    "value": {
        "startDate": null,
        "endDate": null
    },
    "properties": {
        "string": true,
        "relation": false
    }
},

{
    "colName": "dob",
    "label": "Date Time",
    "tableName": "Member",
    "inputType": "dateTime",
    "value": null,
    "properties": {
        "dateTime": true
    }
},

{
    "colName": "dob",
    "label": "Time Range",
    "tableName": "Member",
    "inputType": "dateTimeRange",
    "value": {
        "startDate": null,
        "endDate": null
    },
    "properties": {
        "dateTime": true,
        "relation": false
    }
},

{
    "colName": "salisPoint",
    "label": "Slider",
    "tableName": "Member",
    "inputType": "slider",
    "value": 0,
    "properties": {
        "id": true,
        "isActive": false,
        "config": {
            "connect": false,
            "margin": 1,
            "range": {
                "min": 0,
                "max": 50
            },
            "step": 1
        }
    }
},

{
    "colName": "salisPoint",
    "label": "Slider Range",
    "tableName": "Member",
    "inputType": "sliderRange",
    "value": [0, 50],
    "properties": {
        "id": true,
        "relation": false,
        "isActive": false,
        "config": {
            "connect": true,
            "margin": 1,
            "range": {
                "min": 0,
                "max": 50
            },
            "start": [0, 50]
        }
    }
},


{
    "colName": "status",
    "label": "status",
    "tableName": "Member",
    "inputType": "boolean",
    "value": null,
    "properties": {

    }
},
{
    "colName":"gender",
    "label":"Gender",
    "tableName":"Member",
    "inputType":"radio",
    "value":null,
    "properties": {
        "string": true,
        "id": true,
        "relation": false,
        "data": [{
                "id": 1,
                "name": "male"
            },
            {
                "id": 2,
                "name": "female"
            }
        ],
        "propertyToBind": "id",
        "propertyToShow": "name"
    }

}
]