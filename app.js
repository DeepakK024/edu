var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');


// var usersRouter = require('./routes/user/users');
// const signUp = require ('./routes/user/users');
const getUsers = require('./routes/user/getusers');
const getFilter = require('./routes/user/getfilters');
const delUser = require('./routes/user/deleteuser');
const editUser = require('./routes/user/createUser');
const insertInto = require('./routes/user/insert');
const { signUpValidator } = require('./middleware/middleware');
const uploadProfile = require('./routes/user/uploadVideo');
const defaultVideo = require('./routes/user/defaultVideo');
const fileUpload = require('./routes/features/fileUpload');
const s3fileproxy = require('./routes/features/s3proxy');
const upload = require('./middleware/misc');
const linkUpload = require('./routes/features/linkUpload');
const signup = require('./routes/management/userSignup');
const actions = require('./routes/features/actions');
const login = require('./routes/management/userLogin');
const logout = require('./routes/management/userLogout');

//
const Auth = require('./middleware/authorization/userAuthorization');
const Privilige = require('./middleware/authorization/userPriviliges');

const dataFilling = require('./routes/user/dataFilling');
// const {celebrate,Joi} = require('celebrate');
const { errors } = require('celebrate');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

// app.use('/', indexRouter);
// app.use('/users', usersRouter);
// app.use('/',signUp);
//app.post('/tableview', getUsers); //get values

app.post('/tableview', Auth.authenticateToken, Privilige.isPriviliged, getUsers); //get values
app.post('/getfilters', Auth.authenticateToken, Privilige.isPriviliged, getFilter); //get filters 
app.post('/delete', Auth.authenticateToken, Privilige.isPriviliged, delUser);
app.post('/getform', Auth.authenticateToken, Privilige.isPriviliged, editUser); //fields 
app.post('/createedit', Auth.authenticateToken, Privilige.isPriviliged, insertInto); //save for create or edit
//app.use('/uploadProfile', Auth.authenticateToken, Privilige.isPriviliged, uploadProfile) //video upload
app.post('/defaultVideo', Auth.authenticateToken, defaultVideo) //update default video
app.post('/uploadfile', upload, fileUpload) //uploading a file
app.post('/s3fileproxy', s3fileproxy); //to get file from our s3 bucket.
app.post('/uploadlink', Auth.authenticateToken, linkUpload); //link upload.
app.post('/action', Auth.authenticateToken, Privilige.isPriviliged, actions); //all the actions

// user managment apis:
app.post('/register', signup); //user signup
app.post('/login', login);
app.post('/logout', Auth.authenticateToken, logout);

// app.post('/userinfo',getUser); 

app.post('/fillData', dataFilling);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
